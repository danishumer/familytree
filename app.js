/**
* Module dependencies.
*/
var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , family = require('./routes/family')
  , relationship = require('./routes/relationship')
  , person = require('./routes/person')
  , blog = require('./routes/blog')
  , api = require('./routes/api')
  , http = require('http')
  , path = require('path')
  , fileExists = require('file-exists');
//var methodOverride = require('method-override');
var async = require('async');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var fileUpload = require('express-fileupload');
var fs = require('fs');
var resizeImg = require('resize-img');
var app = express();
var mysql = require('mysql');
var bodyParser = require("body-parser");
var connection = mysql.createConnection({
              host     : '172.30.189.108',
              user     : 'root',
              password : 'root',
              database : 'familytree'
            });

// default options
app.use(fileUpload());

connection.config.queryFormat = function (query, values) {
  if (!values) return query;
  return query.replace(/\:(\w+)/g, function (txt, key) {
    if (values.hasOwnProperty(key)) {
      return this.escape(values[key]);
    }
    return txt;
  }.bind(this));
}

connection.connect();
 
global.db = connection;
global.fileExists = fileExists;
global.async = async;
global.fs = fs;
global.resizeImg = resizeImg;
global.api_key = 'aN(LCyra)r<aejh"M@xWI>+)KS-Jn`W>ZjRpyQV8]wS%Khm#v?FB$E#BQ,+87==';

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 2678400000 }
}));
 
// development only
 
// app.get('/', routes.index);//call for main index page
// app.get('/signup', user.signup);//call for signup page
// app.post('/signup', user.signup);//call for signup post 
app.get("/", function(req, res) {
  res.send('Welcome to the website');
});

app.get('/admin', function(req, res) {
  if(req.session.userId){
    res.redirect('/admin/home/dashboard');
  } else {
    res.redirect('/');
  }
});

app.get('/admin/login', routes.index);//call for login page
app.post('/admin/login', user.login);//call for login post
app.get('/admin/home/dashboard', user.dashboard);//call for dashboard page after login
app.get('/admin/home/logout', user.logout);//call for logout
// app.get('/home/profile',user.profile);//to render users profile

// Families Routes
app.get('/admin/family', family.view);
app.get('/admin/family/add', family.add);
app.post('/admin/family/add', family.add);
app.get('/admin/family/edit/:family_id', family.edit);
app.post('/admin/family/edit/:family_id', family.edit);

// Relationships Routes
app.get('/admin/relationship', relationship.view);
app.get('/admin/relationship/add', relationship.add);
app.post('/admin/relationship/add', relationship.add);
app.get('/admin/relationship/edit/:relationship_id', relationship.edit);
app.post('/admin/relationship/edit/:relationship_id', relationship.edit);

// Person Routes
app.get('/admin/person', person.view);
app.get('/admin/person/add', person.add);
app.post('/admin/person/add', person.add);
app.get('/admin/person/edit/:person_id', person.edit);
app.post('/admin/person/edit/:person_id', person.edit);

// Blog Routes
app.get('/admin/blog', blog.view);
app.get('/admin/blog/add', blog.add);
app.post('/admin/blog/add', blog.add);
app.get('/admin/blog/edit/:blog_id', blog.edit);
app.post('/admin/blog/edit/:blog_id', blog.edit);

// Api Routes
app.get('/api/families', api.families);
app.get('/api/tree/:person_id', api.tree);
app.get('/api/user_card/:person_id', api.user_card);

//Middleware
app.listen(8080)
