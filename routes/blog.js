//---------------------------------------------Blog View------------------------------------------------------
exports.view = function(req, res){
    if(req.session.userId){
        var sql = `SELECT * FROM blog`;
        db.query(sql, function(err, results){
            res.render('blog_view', {data: results});
        });        
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Blog Edit------------------------------------------------------
exports.edit = function(req, res){
    if(req.session.userId){
        var message = '';
        if(req.method == "POST"){
            var sql="update relationship set person1_id = :person1_id, person2_id = :person2_id where relationship_id = :relationship_id";
            db.query(sql, {person1_id: req.body.person_1, person2_id: req.body.person_2, relationship_id: req.body.relationship_id}, function(err, results){      
                res.redirect('/admin/relationship');
            });
        } else {
            if(req.params.relationship_id != '') {

                var males, females;
                async.parallel([
                    // Get Males
                    function(callback) {
                        var sql = "SELECT person_id, first_name FROM person WHERE gender = 'm'";
                        db.query(sql, function(err, results){
                            males = results;
                            callback();
                        });
                    },
                    // Get Females
                    function(callback) {
                        var sql = "SELECT person_id, first_name FROM person WHERE gender = 'f'";
                        db.query(sql, function(err, results){
                            females = results;
                            callback();
                        });
                    }
                ], function(err) { 
                    if (err) {
                        console.log(err.stack);
                    }
                    
                    var sql="SELECT * FROM relationship where relationship_id = :relationship_id";                           
                    db.query(sql, {relationship_id: req.params.relationship_id}, function(err, results){      
                        res.render('relationship_edit', {
                            data: results[0],
                            males: males, 
                            females: females,
                        });
                    });
                });
                
            } else {
                res.redirect('/admin/relationship');    
            }
        }
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Blog Add------------------------------------------------------
 exports.add = function(req, res){
    if(req.session.userId){
        if(req.method == "POST"){
            var sql="insert into blog set person_id = :person_id, blog_title = :blog_title, blog_desc = :blog_desc";
            db.query(sql, {person_id: req.body.person_id, blog_title: req.body.title, blog_desc: req.body.description}, function(err, results){      
                res.redirect('/admin/blog');
            });            
        } else {

            var persons;
            async.parallel([
                // Get Persons
                function(callback) {
                    var sql = "SELECT person_id, first_name FROM person";
                    db.query(sql, function(err, results){
                        persons = results;
                        callback();
                    });
                }
            ], function(err) { 
                if (err) {
                    console.log(err.stack);
                }
                res.render('blog_add', {
                    persons: persons
                });
            });

        }
    } else {
        res.redirect('/admin/login');
    }
 };