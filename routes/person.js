//---------------------------------------------Person View------------------------------------------------------
exports.view = function(req, res){
    if(req.session.userId){
        var sql = "SELECT person_id, first_name, IF(gender = 'm', 'Male', 'Female') AS gender, ";
            sql += "(SELECT first_name FROM person WHERE person_id = r.`person1_id`) AS father, ";
            sql += "(SELECT first_name FROM person WHERE person_id = r.`person2_id`) AS mother, family_name, "; 
            sql += "CONCAT(`birth_day`, '-', `birth_month`, '-', `birth_year`) AS date_of_birth ";
            sql += "FROM person AS p ";
            sql += "INNER JOIN family_name AS fn ON fn.`family_name_id` = p.`family_name_id` ";
            sql += "LEFT JOIN `relationship` AS r ON r.`relationship_id` = p.`parent_relationship_id`";                           
        db.query(sql, function(err, results){
            results.forEach(function(row, index, theArray) {                
                theArray[index].hasThumb = fileExists.sync('./images/person/' + theArray[index].person_id + '-thumb.jpg', {root: 'public'});
            });
            res.render('person_view', {data: results});
        });        
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Person Edit------------------------------------------------------
exports.edit = function(req, res){
    if(req.session.userId){
        if(req.method == "POST"){
            var personId = req.body.person_id;
            async.series([
                // Save person data
                function(callback) {
                    var sql = `
                        update person 
                        set 
                            first_name = :first_name,
                            first_name_search = :first_name_search,
                            family_name_id = :family,
                            distinguish_name = :name_of_distinction,
                            distinguish_name_search = :distinguish_name_search,
                            parent_relationship_id = :parent,
                            family_tree_root = :head_of_family,
                            gender = :gender,
                            birth_day = :birth_day,
                            birth_month = :birth_month,
                            birth_year = :birth_year,
                            birth_hour = :birth_hour,
                            birth_minute = :birth_minute,
                            birth_timezone = :birth_timezone,
                            dead = :dead,
                            death_day = :death_day,
                            death_month = :death_month,
                            death_year = :death_year,
                            biography = :biography,
                            city_id = :city_of_birth,
                            birth_longitude = :birth_longitude,
                            birth_latitude = :birth_latitude,
                            birth_map_zoom = :birth_map_zoom 
                        where person_id = :person_id
                    `;

                    var birth_day = null;
                    var birth_month = null;
                    var birth_year = null;
                    
                    if(req.body.date_of_birth != ''){
                        var arrDob = req.body.date_of_birth.split("/");
                        birth_day = arrDob[1];
                        birth_month = arrDob[0];
                        birth_year = arrDob[2];
                    }

                    var death_day = null;
                    var death_month = null;
                    var death_year = null;

                    if(req.body.date_of_death != ''){
                        var arrDeath = req.body.date_of_death.split("/");
                        death_day = arrDeath[1];
                        death_month = arrDeath[0];
                        death_year = arrDeath[2];
                    }

                    if(req.body.birth_hour == ''){
                        req.body.birth_hour = null;
                    }

                    if(req.body.birth_minute == ''){
                        req.body.birth_minute = null;                        
                    }

                    if(req.body.birth_timezone == ''){
                        req.body.birth_timezone = null;
                    }

                    if(req.body.city_of_birth == ''){
                        req.body.city_of_birth = null;
                    }

                    db.query(sql, {
                        first_name: req.body.first_name,
                        first_name_search: req.body.first_name,
                        family: req.body.family,
                        name_of_distinction: req.body.name_of_distinction,
                        distinguish_name_search: req.body.name_of_distinction,
                        parent: req.body.parent,
                        head_of_family: req.body.head_of_family,
                        gender: req.body.gender,
                        birth_day: birth_day,
                        birth_month: birth_month,
                        birth_year: birth_year,
                        birth_hour: req.body.birth_hour,
                        birth_minute: req.body.birth_minute,
                        birth_timezone: req.body.birth_timezone,
                        dead: req.body.dead,
                        death_day: death_day,
                        death_month: death_month,
                        death_year: death_year,
                        biography: req.body.biography,
                        city_of_birth: req.body.city_of_birth,
                        birth_longitude: req.body.birth_longitude,
                        birth_latitude: req.body.birth_latitude,
                        birth_map_zoom: req.body.birth_map_zoom,
                        person_id: personId
                    }, function(err, results){
                        if (err) {
                            console.log(err.stack);
                        }
                        callback();
                    });
                },
                // Del links
                function(callback) {
                    var sql = 'delete from person_url where person_id = :person_id';
                    db.query(sql, {
                        person_id: personId 
                    }, function(err, results){
                        if (err) {
                            console.log(err.stack);
                        }
                        callback();
                    });
                },
                // Save links
                function(callback) {
                    console.log(req.body.url);
                    var sql = `
                        insert into person_url 
                        set 
                            url = :url,
                            person_id = :person_id
                    `;
                    if(Array.isArray(req.body.url)){
                        req.body.url.forEach(function(row, index, theArray) {                
                            db.query(sql, {
                                url: theArray[index],
                                person_id: personId 
                            }, function(err, results){
                                if (err) {
                                    console.log(err.stack);
                                }
                            });
                        });
                    } else if(req.body.url != undefined) {
                        db.query(sql, {
                            url: req.body.url,
                            person_id: personId 
                        }, function(err, results){
                            if (err) {
                                console.log(err.stack);
                            }
                        });
                    }
                    callback();
                },
                // Save Image
                function(callback) {
                    console.log(req.files.profilepic);
                    if (req.files.profilepic != undefined){
                        var profilepic = req.files.profilepic;
                        profilepic.mv('./public/images/person/' + personId + '.jpg', function(err) {
                            if (err) {
                                console.log(err.stack);
                            } else {
                                resizeImg(fs.readFileSync('./public/images/person/' + personId + '.jpg'), {width: 151, height: 200}).then(buf => {
                                    fs.writeFileSync('./public/images/person/' + personId + '-thumb.jpg', buf);
                                    callback();
                                });
                            }
                        });
                    } else {
                        callback();
                    }                    
                }
            ], function(err) { 
                if (err) {
                    console.log(err.stack);
                }
                res.redirect('/admin/person');
            });

        } else {
            
            if(req.params.person_id != '') {

                var cities, families, parents, person_data, urls;
                var person_id = req.params.person_id;
                
                async.parallel([
                    // Get cities
                    function(callback) {
                        var sql = "select * from city";
                        db.query(sql, function(err, results){
                            cities = results;            
                            callback();
                        });
                    },
                    // Get families
                    function(callback) {
                        var sql = "select * from family_name group by family_name";
                        db.query(sql, function(err, results){
                            families = results;
                            callback();
                        });
                    },
                    // Get Parents
                    function(callback) {
                        var sql = `SELECT * FROM (
                            SELECT 
                                relationship_id, 
                                ( SELECT first_name FROM person WHERE person_id = person1_id ) AS father, 
                                ( SELECT first_name FROM person WHERE person_id = person2_id ) AS mother 
                            FROM relationship
                        ) AS parents WHERE father IS NOT NULL AND mother IS NOT NULL`;
                        db.query(sql, function(err, results){
                            parents = results;
                            callback();
                        });
                    },
                    // Get Person Record
                    function(callback) {
                        var sql = `SELECT * FROM person where person_id = :person_id`;
                        db.query(sql, {
                            person_id: person_id
                        }, function(err, results){
                            person_data = results[0];
                            callback();
                        });
                    },
                    // Get Person Urls
                    function(callback) {
                        var sql = `SELECT * FROM person_url where person_id = :person_id`;
                        db.query(sql, {
                            person_id: person_id
                        }, function(err, results){
                            urls = results;
                            callback();
                        });
                    }
                ], function(err) { 
                    if (err) {
                        console.log(err.stack);
                    }
                    res.render('person_edit', {
                        cities: cities, 
                        families: families,
                        parents: parents,
                        person_data: person_data,
                        urls: urls
                    });
                }); 
            } else {
                res.redirect('/admin/person');
            }
        }
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Person Add------------------------------------------------------
 exports.add = function(req, res){
    if(req.session.userId){
        var message = '';
        if(req.method == "POST"){
            var personId;
            async.series([
                // Save person data
                function(callback) {
                    var sql = `
                        insert into person 
                        set 
                            first_name = :first_name,
                            first_name_search = :first_name_search,
                            family_name_id = :family,
                            distinguish_name = :name_of_distinction,
                            distinguish_name_search = :distinguish_name_search,
                            parent_relationship_id = :parent,
                            family_tree_root = :head_of_family,
                            gender = :gender,
                            birth_day = :birth_day,
                            birth_month = :birth_month,
                            birth_year = :birth_year,
                            birth_hour = :birth_hour,
                            birth_minute = :birth_minute,
                            birth_timezone = :birth_timezone,
                            dead = :dead,
                            death_day = :death_day,
                            death_month = :death_month,
                            death_year = :death_year,
                            biography = :biography,
                            city_id = :city_of_birth,
                            birth_longitude = :birth_longitude,
                            birth_latitude = :birth_latitude,
                            birth_map_zoom = :birth_map_zoom
                    `;

                    var birth_day = null;
                    var birth_month = null;
                    var birth_year = null;
                    
                    if(req.body.date_of_birth != ''){
                        var arrDob = req.body.date_of_birth.split("/");
                        birth_day = arrDob[1];
                        birth_month = arrDob[0];
                        birth_year = arrDob[2];
                    }

                    var death_day = null;
                    var death_month = null;
                    var death_year = null;

                    if(req.body.date_of_death != ''){
                        var arrDeath = req.body.date_of_death.split("/");
                        death_day = arrDeath[1];
                        death_month = arrDeath[0];
                        death_year = arrDeath[2];
                    }

                    if(req.body.birth_hour == ''){
                        req.body.birth_hour = null;
                    }

                    if(req.body.birth_minute == ''){
                        req.body.birth_minute = null;                        
                    }

                    if(req.body.birth_timezone == ''){
                        req.body.birth_timezone = null;
                    }

                    if(req.body.city_of_birth == ''){
                        req.body.city_of_birth = null;
                    }

                    db.query(sql, {
                        first_name: req.body.first_name,
                        first_name_search: req.body.first_name,
                        family: req.body.family,
                        name_of_distinction: req.body.name_of_distinction,
                        distinguish_name_search: req.body.name_of_distinction,
                        parent: req.body.parent,
                        head_of_family: req.body.head_of_family,
                        gender: req.body.gender,
                        birth_day: birth_day,
                        birth_month: birth_month,
                        birth_year: birth_year,
                        birth_hour: req.body.birth_hour,
                        birth_minute: req.body.birth_minute,
                        birth_timezone: req.body.birth_timezone,
                        dead: req.body.dead,
                        death_day: death_day,
                        death_month: death_month,
                        death_year: death_year,
                        biography: req.body.biography,
                        city_of_birth: req.body.city_of_birth,
                        birth_longitude: req.body.birth_longitude,
                        birth_latitude: req.body.birth_latitude,
                        birth_map_zoom: req.body.birth_map_zoom        
                    }, function(err, results){
                        if (err) {
                            console.log(err.stack);
                        }
                        personId = results.insertId;            
                        callback();
                    });
                },
                // Save links
                function(callback) {
                    console.log(req.body.url);
                    var sql = `
                        insert into person_url 
                        set 
                            url = :url,
                            person_id = :person_id
                    `;
                    if(Array.isArray(req.body.url)){
                        req.body.url.forEach(function(row, index, theArray) {                
                            db.query(sql, {
                                url: theArray[index],
                                person_id: personId 
                            }, function(err, results){
                                if (err) {
                                    console.log(err.stack);
                                }
                            });
                        });
                    } else if(req.body.url != undefined) {
                        db.query(sql, {
                            url: req.body.url,
                            person_id: personId 
                        }, function(err, results){
                            if (err) {
                                console.log(err.stack);
                            }
                        });
                    }
                    callback();
                },
                // Save Image
                function(callback) {
                    if (req.files.profilepic != undefined){
                        var profilepic = req.files.profilepic;
                        profilepic.mv('./public/images/person/' + personId + '.jpg', function(err) {
                            if (err) {
                                console.log(err.stack);
                            } else {
                                resizeImg(fs.readFileSync('./public/images/person/' + personId + '.jpg'), {width: 151, height: 200}).then(buf => {
                                    fs.writeFileSync('./public/images/person/' + personId + '-thumb.jpg', buf);
                                    callback();
                                });
                            }
                        });
                    } else {
                        callback();
                    }
                }
            ], function(err) { 
                if (err) {
                    console.log(err.stack);
                }
                res.redirect('/admin/person');
            });
                        
        } else {
            var cities, families, parents;
            async.parallel([
                // Get cities
                function(callback) {
                    var sql = "select * from city";
                    db.query(sql, function(err, results){
                        cities = results;
                        callback();
                    });
                },
                // Get families
                function(callback) {
                    var sql = "select * from family_name group by family_name";
                    db.query(sql, function(err, results){
                        families = results;
                        callback();
                    });
                },
                // Get Parents
                function(callback) {
                    var sql = `SELECT * FROM (
                        SELECT 
                            relationship_id, 
                            ( SELECT first_name FROM person WHERE person_id = person1_id ) AS father, 
                            ( SELECT first_name FROM person WHERE person_id = person2_id ) AS mother 
                        FROM relationship
                    ) AS parents WHERE father IS NOT NULL AND mother IS NOT NULL`;
                    db.query(sql, function(err, results){
                        parents = results;
                        callback();
                    });
                }
            ], function(err) { 
                if (err) {
                    console.log(err.stack);
                }
                res.render('person_add', {
                    message: message, 
                    cities: cities, 
                    families: families,
                    parents: parents
                });
            });
            // res.render('person_add', {message: message, cities: cities});            
        }
    } else {
        res.redirect('/admin/login');
    }
 };


