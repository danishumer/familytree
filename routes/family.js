//---------------------------------------------Family View------------------------------------------------------
exports.view = function(req, res){
    if(req.session.userId){
        var sql="SELECT * FROM family_name";                           
        db.query(sql, function(err, results){      
            res.render('family_view', {data: results});
        });        
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Family Edit------------------------------------------------------
exports.edit = function(req, res){
    if(req.session.userId){
        var message = '';
        if(req.method == "POST"){
            if(req.body.family_name != ''){
                var sql="update family_name set family_name = :family_name, family_name_search = :family_name_search where family_name_id = :family_id";                           
                db.query(sql, {family_name: req.body.family_name, family_name_search: req.body.family_name, family_id: req.body.family_id}, function(err, results){      
                    res.render('family_edit', {family: results[0]});
                });
            } else {
                message = "Please enter Family name";
                res.render('family_edit', {family: {family_name_id: req.body.family_id}, message: message});
            }
        } else {
            if(req.params.family_id != '') {
                var sql="SELECT * FROM family_name where family_name_id = :family_id";                           
                db.query(sql, {family_id: req.params.family_id}, function(err, results){      
                    res.render('family_edit', {family: results[0], message: message});
                });
            } else {
                res.redirect('/admin/family');    
            }
        }
    } else {
        res.redirect('/admin/login');
    }
 };

 //---------------------------------------------Family Add------------------------------------------------------
 exports.add = function(req, res){
    if(req.session.userId){
        var message = '';
        if(req.method == "POST"){
            if(req.body.family_name != ''){
                var sql="insert into family_name set family_name = :family_name, family_name_search = :family_name_search";
                db.query(sql, {family_name: req.body.family_name, family_name_search: req.body.family_name}, function(err, results){      
                    res.redirect('/admin/family');
                });
            } else {
                message = "Please enter Family name";
                res.render('family_add', {message: message});
            }
        } else {                  
            res.render('family_add', {message: message});            
        }
    } else {
        res.redirect('/admin/login');
    }
 };