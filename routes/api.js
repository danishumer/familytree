/*
* GET Families.
*/
 
exports.families = function(req, res){
    if(api_key == req.headers['x-access-api']){
        var sql = `SELECT fn.family_name_id AS id, fn.family_name AS name, p.person_id FROM family_name AS fn
            INNER JOIN person AS p ON fn.family_name_id = p.family_name_id
            WHERE p.family_tree_root = 1`;
        db.query(sql, function(err, results){
            res.json(results);
        });
    } else {
        res.json(['Invalid Api Key']);
    }
};

exports.tree = function(req, res){
    if(req.params.person_id != '' && api_key == req.headers['x-access-api']) {
        var sql = `SELECT p.person_id, p.first_name, person2_id AS wife_id, (SELECT first_name FROM person WHERE person_id = person2_id) AS wife, 
            p2.person_id AS child_id, p2.first_name AS child_name
            FROM person AS p
            INNER JOIN relationship AS r ON p.person_id = r.person1_id
            LEFT JOIN person AS p2 ON r.relationship_id = p2.parent_relationship_id
            WHERE p.person_id = :person_id`;
        
        var sql = "select person_id as id, first_name as name, gender from person where person_id = :person_id";
        db.query(sql, {person_id: req.params.person_id}, function(err, results){
            
            if( fileExists.sync('./images/person/' + results[0].id + '-thumb.jpg', {root: 'public'}) ) {
                results[0].thumb = './images/person/' + results[0].id + '-thumb.jpg';
            } else {
                results[0].thumb = false;
            }
            
            var wifeSql = `SELECT person_id AS id, first_name AS name, gender FROM person AS p
            INNER JOIN relationship AS r ON p.person_id = r.person2_id
            WHERE r.person1_id = :person1_id`;
            db.query(wifeSql, {person1_id: results[0].id}, function(err, results2){
                if(results2.length > 0){
                    results[0].relation = results2;
                    results2.forEach(function(row, index, theArray) {                
                        // theArray[index].hasThumb = fileExists.sync('./images/person/' + theArray[index].person_id + '-thumb.jpg', {root: 'public'});

                        if( fileExists.sync('./images/person/' + theArray[index].id + '-thumb.jpg', {root: 'public'}) ) {
                            theArray[index].thumb = './images/person/' + theArray[index].id + '-thumb.jpg';
                        } else {
                            theArray[index].thumb = false;
                        }

                        var childSql = `SELECT person_id AS id, first_name AS name, gender FROM person AS p
                        INNER JOIN relationship AS r ON p.parent_relationship_id = r.relationship_id
                        WHERE r.person2_id = :mother_id`;
                        db.query(childSql, {mother_id: theArray[index].id}, function(err, results3){
                            results3.forEach(function(row3, index3, theArray3) {
                                if( fileExists.sync('./images/person/' + theArray3[index3].id + '-thumb.jpg', {root: 'public'}) ) {
                                    theArray3[index3].thumb = './images/person/' + theArray3[index3].id + '-thumb.jpg';
                                } else {
                                    theArray3[index3].thumb = false;
                                }       
                            });
                            theArray[index].children = results3;
                            if(results2.length == index+1){
                                res.json(results);
                            }
                        });
                    });
                } else {
                    res.json(results);
                }
            });
        });
    } else {
        res.json(['Invalid Api Key']);
    }
};

exports.user_card = function(req, res){
    if(req.params.person_id != '' && api_key == req.headers['x-access-api']) {
        var sql = `SELECT first_name AS name, family_name, biography FROM person AS p
            INNER JOIN family_name AS fn ON fn.family_name_id = p.family_name_id
            WHERE person_id = :person_id`;
        db.query(sql, {person_id: req.params.person_id}, function(err, results){
            res.json(results);
        });
    } else {
        res.json(['Invalid Api Key']);
    }
};