var marker = null;
function initMap() {
	var zoom = $("#birth_map_zoom").val();
	var longitude = $("#birth_longitude").val();
	var latitude = $("#birth_latitude").val();
	
	zoom = zoom == ""? 8:parseInt(zoom);
	longitude = longitude == ""? 54.2797112:parseFloat(longitude);

	latitude = latitude == ""? 24.3862904:parseFloat(latitude);
	
	var loc = {lat: latitude, lng: longitude};


	map = new google.maps.Map(document.getElementById('map'), {
		zoom: zoom,
		center: loc

	});

	
	createMarker(map,loc );



	
	$("#gmap_zoom").val(8);

	google.maps.event.addDomListener(map,'zoom_changed', function() {
		$("#birth_map_zoom").val(map.getZoom());

	});


	google.maps.event.addListener(map, "click", function (e) {
		createMarker(map,e.latLng);
	});


	var geocoder = new google.maps.Geocoder();

	$('#gmap-geocoding').css({"visibility":"visible","top":$("#map").position().top+"px","right":$("#map").position().right+"px"});
	$('#address').on('keypress', function (e) {
		if(e.which === 13){
			geocodeAddress(geocoder, map);
		}});
}

function geocodeAddress(geocoder, resultsMap) {

	var address = document.getElementById('address').value;
	geocoder.geocode({'address': address}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);

			createMarker(resultsMap,results[0].geometry.location);

		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}

function createMarker(map,latLng) {
	if(marker!=null){
		marker.setMap(null);
	}

	populateLatLngToForm(latLng);


	marker = new google.maps.Marker({
		map: map,
		draggable:true,
		position: latLng
	});


	google.maps.event.addListener(
		marker,
		'drag',
		function() {
			populateLatLngToForm(marker.position);
		}


		);


}

function populateLatLngToForm(latLng){
	$("#birth_longitude").val(latLng.lng);
	$("#birth_latitude").val(latLng.lat);
	$("#birth_map_zoom").val(map.getZoom());

}